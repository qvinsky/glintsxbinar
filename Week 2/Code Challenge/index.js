const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}
// Should return array
function sortAscending(data) {
  // Code Here
  var data = clean(data)
  let len = data.length;
  let swapped;
  do {
    swapped = false;
    for (let i = 0; i < len; i++) {
      if (data[i] > data[i + 1]) {
        let temp = data[i];
        data[i] = data[i + 1];
        data[i + 1] = temp;
        swapped = true;
      }
    }
  } while (swapped);
  return data;
}



// Should return array
function sortDecending(data) {
  // Code Here
  var data = clean(data)
  let len = data.length;
  let swapped;
  do {
    swapped = false;
    for (let i = 0; i < len; i++) {
      if (data[i] < data[i + 1]) {
        let temp = data[i];
        data[i] = data[i + 1];
        data[i + 1] = temp;
        swapped = true;
      }
    }
  } while (swapped);
  return data;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
