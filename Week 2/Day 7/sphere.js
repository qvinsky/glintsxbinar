/**
 * TIDAK SEMUA ADA VALIDASI isNaN
 * operation==1 artinya mencari luas permukaan
 * operation==2 artinya mencari volume
 * utk CONE dan SPHERE ga ada fungsinya disini udah terlanjur buat di task kmren
 */
const index = require('./index.js')

function volumeOfSphere(r){
    const result=(4/3)*(3.14)*(r*r*r);
    return "Result = "+result;
}

function surfaceAreaOfSphere(r){
    const result=4*(3.14)*(r**2)
    return "Result = "+result
}
//INPUT FOR SPHERE
function inputRadius(operation) {
    index.rl.question("radius: ", rad => {
        if (!isNaN(rad)) {
            if(operation==1){
                console.log(surfaceAreaOfSphere(rad))
                console.log("======================================");
                index.rl.close()
            }else if(operation==2){
                console.log(volumeOfSphere(rad))
                console.log("======================================");
                index.rl.close()
            }
        } else {
            console.log(`Radius must be a number`)
            inputRadius(operation)
        }
    })
  }
  module.exports.sphere = inputRadius;