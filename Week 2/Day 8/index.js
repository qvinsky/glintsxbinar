const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

//import file path
const masak = require ("./masak.js");
const belanja = require ("./belanja.js");
const mateng = require (`./mateng.js`)
const kompormasak = require (`./kompormasak.js`)

let kitchen = []
let kompor = []
let tingkatkematangan = []

function masaknasigoreng(){
  siapinwajan()
  let bahanhabis = checkbahan()
  BelanjaDulu(bahanhabis)
  let gashabis = checkkompor()
  gantigas(gashabis)
  osengoseng()
  osenglagi()
  let masihbelummateng = cekmateng()
  osengoseng2(masihbelummateng)
  tahapakhir()
}

masaknasigoreng()

module.exports.rl = rl;