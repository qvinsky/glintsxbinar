const index = require('./index.js')

function siapinwajan(){
    console.log(`Siapin wajan`);
  }
  function osengoseng(){
      console.log(`Taruh wajan diatas kompor`)
      console.log(`panaskan minyak`)
      console.log(`tumis telur dan bumbu`)
      console.log(`masukan nasi`)
  }
  function osenglagi(){
      console.log(`campur dan di aduk`)
  }
  function tahapakhir(){
      console.log(`matikan kompor`)
      console.log(`siapin piring`)
      console.log(`taro nasi ke atas piring`)
  }

  module.exports.siapinwajan = siapinwajan;
  module.exports.osengoseng = osengoseng;
  module.exports.osenglagi = osenglagi;
  module.exports.tahapakhir = tahapakhir;