const index = require('./index.js')

function cookAnEgg() {
    // Check if the egg is available
    let shouldWeGoToTheMarket = checkTheEggAvailability();
    
    // Check if we should go to the market or not
    goToTheMarket(shouldWeGoToTheMarket);
    prepareTheFry()
    fryTheEgg();
  }
  
  cookAnEgg()
  
module.exports.nasigoreng = nasigoreng;