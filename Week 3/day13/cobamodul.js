// import modul
const modul = require('./module/module.js');

// instansiasi modul
const hitung = new modul()

// Use hitung
console.log(hitung.menghitungLuasPersegi(11))
console.log(hitung.menghitungLuasPersegiPanjang(20, 20));
console.log(hitung.menghitungKelilingPersegi(40));
console.log(hitung.menghitungKelilingPersegiPanjang(100, 50));
console.log(hitung.menghitungVolumeKubus(20));

console.log(hitung.menghitungLuasLingkaran(14));
console.log(hitung.menghitungKelilingLingkaran(21));
console.log(hitung.menghitungVolumeBola(7));
console.log(hitung.menghitungVolumeKerucut(21,20));
console.log(hitung.menghitungLuasPermukaanBola(10))
console.log(hitung.menghitungLuasPermukaanKerucut(21,20))

// Persegi 1
let sisi = 50
let luaspersegi1 = hitung.menghitungLuasPersegi(sisi)
let kelilingpersegi1 = hitung.menghitungKelilingPersegi(sisi)

console.log(luaspersegi1, kelilingpersegi1);
