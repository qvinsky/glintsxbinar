// import parent class
const bangunruang = require('./bangunruang.js')

class Kerucut extends bangunruang {
  constructor(radius,height) {
    super('Kerucut')

    this.radius = radius
    this.height=height
    this.slant= Math.sqrt(Math.pow(radius,2)+Math.pow(height,2))
  }

  // Override menghitungVolume
  menghitungVolume() {
    //   super.menghitungVolume()
    return Math.PI * this.height * Math.pow(this.radius, 2) / 3
  }

  // Override menghitungLuas Permukaan
  menghitungLuasPermukaan() {
    //   super.menghitungLuasPermukaan()
      return Math.PI * this.radius*(this.slant+this.radius)
  }
}

module.exports = Kerucut;
