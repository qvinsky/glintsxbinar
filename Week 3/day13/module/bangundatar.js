// This is class BangunDatar
class BangunDatar {
  constructor(name) {
    // It is abstract class
    if (this.constructor === BangunDatar) {
      throw new Error('This is abstract')
    }

    this.name = name
  }

  // instance method
  menghitungLuas() {
    console.log(`Menghitung Luas`);
  }

  // instance method
  menghitungKeliling() {
    console.log(`Menghitung Keliling`);
  }
}

module.exports = BangunDatar;
