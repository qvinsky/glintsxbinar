// This is class BangunRuang
class BangunRuang {
    constructor(name) {
      // It is abstract class
      if (this.constructor === BangunRuang) {
        throw new Error('This is abstract')
      }
  
      this.name = name
    }
  
    // instance method
    menghitungVolume() {
      console.log(`Menghitung Volume`);
    }
  
    // instance method
    // menghitungLuasPermukaan() {
    //   console.log(`Menghitung Luas Permukaan`);
    // }
  }
  
  module.exports = BangunRuang;
  