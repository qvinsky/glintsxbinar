// import parent class
const bangunruang = require('./bangunruang.js')

class Bola extends bangunruang {
  constructor(radius) {
    super('Bola')

    this.radius = radius
  }

  // Override menghitungVolume
  menghitungVolume() {
    //   super.menghitungVolume()
    return 4*Math.PI * Math.pow(this.radius, 3)/3
  }

  // Override menghitungLuas Permukaan
  menghitungLuasPermukaan() {
    //   super.menghitungLuasPermukaan()
    return 4 * Math.PI * Math.pow(this.radius, 2)
  }
}

module.exports = Bola;
