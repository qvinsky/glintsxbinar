// Import bangun datar all class
const persegi = require('./persegi.js');
const persegipanjang = require('./persegipanjang.js');
const kubus = require(`./kubus.js`);
const lingkaran = require('./Lingkaran.js')
const bola = require('./bola.js')
const kerucut = require('./Kerucut.js')

/* Start make module */
class Module {
  constructor() {
    this.name = 'Ini modul'
  }

  menghitungLuasLingkaran(radius) {
    let lingkaranHitung = new lingkaran(radius) // make object of lingkaran
    return lingkaranHitung.menghitungLuas() // calculate area of lingkaran
  }

  menghitungKelilingLingkaran(radius) {
    let lingkaranHitung = new lingkaran(radius) // make object of lingkaran
    return lingkaranHitung.menghitungKeliling()
  }

  menghitungLuasPermukaanBola(radius){
    let bolaHitung=new bola(radius)
    return bolaHitung.menghitungLuasPermukaan()
  }

  menghitungVolumeBola(radius){
    let bolaHitung=new bola(radius)
    return bolaHitung.menghitungVolume()
  }

  menghitungLuasPermukaanKerucut(radius,height){
    let kerucutHitung=new kerucut(radius,height)
    return kerucutHitung.menghitungLuasPermukaan()
  }

  menghitungVolumeKerucut(radius,height){
    let kerucutHitung=new kerucut(radius,height)
    return kerucutHitung.menghitungVolume()
  }

  menghitungVolumeKubus(sisi2) {
    let kubusHitung = new kubus(sisi2) // make object of kubus
    return kubusHitung.menghitungVolume() // calculate area of kubus
  }

  menghitungLuasPersegi(sisi) {
    let persegiHitung = new persegi(sisi) // make object of persegi
    return persegiHitung.menghitungLuas() // calculate area of persegi
  }

  menghitungKelilingPersegi(sisi) {
    let persegiHitung = new persegi(sisi) // make object of persegi
    return persegiHitung.menghitungKeliling() // calculate perimeter of persegi
  }

  menghitungLuasPersegiPanjang(panjang, lebar) {
    let persegiPanjangHitung = new persegipanjang(panjang, lebar) // make object of persegi panjang
    return persegiPanjangHitung.menghitungLuas() // calculate area of persegi panjang
  }

  menghitungKelilingPersegiPanjang(panjang, lebar) {
    let persegiPanjangHitung = new persegipanjang(panjang, lebar) // // make object of persegi panjang
    return persegiPanjangHitung.menghitungKeliling() // calculate perimeter of persegi panjang
  }
}
/* end make module */

module.exports = Module;
