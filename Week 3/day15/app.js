const express = require('express')
const app = express()
const kevinRoutes = require('./routes/kevinRoutes.js')

app.use(express.static('public'));

app.use('/', kevinRoutes);

app.use('/kevin', kevinRoutes);

app.listen(9090);
