const express = require('express');
const router = express.Router()
const KevinController = require('../controllers/kevinController.js')

router
.get('/', KevinController.home)
.get('/kevin', KevinController.kevinHome)

module.exports = router;
