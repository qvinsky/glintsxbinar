const { Transaksi, Barang, Pelanggan, Pemasok } = require("../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator'); //form validation & sanitize form params

class BarangController {

  // constructor() {
  //   Barang.hasMany(Transaksi, {
  //     foreignKey: 'id_barang'
  //   })
  //   Pelanggan.hasMany(Transaksi, {
  //     foreignKey: 'id_pelanggan'
  //   })
  //   Transaksi.belongsTo(Barang, {
  //     foreignKey: 'id_barang'
  //   })
  //   Transaksi.belongsTo(Pelanggan, {
  //     foreignKey: 'id_barang'
  //   })
  // }
  // Get All data from transaksi
    async getAll(req, res) {
      Barang.findAll().then(barang => {
        res.json(barang) // Send response JSON and get all of Transaksi table
      })
    }
    // Get One data from barang
  async getOne(req, res) {
    Barang.findOne({ // find one data of Barang table
      where: {
        id: req.params.id // where id of Barang table is equal to req.params.id
      },
    }).then(barang => {
      res.json(barang) // Send response JSON and get one of Barang table depend on req.params.id
    })
  }
  // Create Barang data
  async create(req, res) {
    barang.create({
        nama: req.body.nama,
        harga: req.body.harga,
        id_pemasok: req.body.id_pemasok,
        image: req.file === undefined ? "" : req.file.filename
      })
      .then(newBarang => {
        // Send response JSON and get one of Transaksi table that we've created
        res.json({
          "status": "success",
          "message": "barang added",
          "data": newBarang
        })
      })
  }

  // Update Transaksi data
  async update(req, res) {
    const update = {
      nama: req.body.nama,
      harga: req.body.harga,
      id_pemasok: req.body.id_pemasok,
      image: req.file === undefined ? "" : req.file.filename
    }
    barang.update(update, {
      where: {
        id: req.params.id
      }
    })
  .then(affectedRow => {
    if (affectedRow) {
      return barang.findOne({ // find one data of Transaksi table
        where: {
          id: req.params.id // where id of Transaksi table is equal to req.params.id
        }
      })
    }
  }).then(b => {
    res.json({
      "status": "success",
      "message": "barang updated",
      "data": b
    })
  })
}
  // Soft delete Transaksi data
  async delete(req, res) {
    barang.destroy({  // Delete data from Transaksi table
      where: {
        id: req.params.id  // Where id of Transaksi table is equal to req.params.id
      }
    }).then(affectedRow => {
      // If delete success, it will return this JSON
      if (affectedRow) {
        return {
          "status": "success",
          "message": "barang deleted",
          "data": null
        }
      }
      // If failed, it will return this JSON
      return {
        "status": "error",
        "message": "Failed",
        "data": null
      }
    }).then(r => {
      res.json(r) // Send response JSON depends on failed or success
    })
  }
}


module.exports = new BarangController;
