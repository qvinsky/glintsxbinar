const {
  Transaksi,
  Barang,
  Pelanggan,
  Pemasok
} = require("../../models") // Import models
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

module.exports = {
  create: [
    check('id').isNumeric().custom(value => {
      return Pemasok.findOne({
        where: {
          id: value
        }
      }).then(b => {
        if (!b) {
          throw new Error('ID pemasok tidak ada!');
        }
      })
    }),
    check('name').isLength({
      min: 2
    }), (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],

  update: [
    //Set form validation rule
    check('id').isLength({
      min: 1
    }).isNumeric().custom(value => {
      return Pemasok.findOne({
        where: {
          id: value
        }
      }).then(b => {
        if (!b) {
          throw new Error('ID pemasok tidak ada!');
        }
      })
    }),
    check('name').isLength({
      min: 2
    }), (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  delete: [
    check('id').isLength({
      min: 1
    }).isNumeric().custom(value => {
      return Pemasok.findOne({
          where: {
            id: value
          }
        })
        .then(p => {
          if (!p) {
            throw new Error('ID tidak ditemukan');
          }
        })
    })
  ]
};
